/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package squareminimiumproblem;

import java.util.InputMismatchException;
import java.util.Scanner;


/**
 *
 * @author Caili
 */
public class Squareminimiumproblem {
    
    //Matriz A
    
   static  int TAMX=2; //parametros de tamaño con la interfaz
  static   int TAMY=2;
  
  
  static int MATRIZ[][]=new int [TAMX][TAMY];
 static  int i,j;
    
  static  int contMatrizColumna=0;
    
    
    //Columna b
   static  int COLUM=2;// parametro de la interfaz
   
   
   static  int COLUMNA[][]=new int [COLUM][COLUM];
   static int ic;
    
  static   int contColumna=0;

    
   static int bit;
   
   static int MATRIZASOCIADA[][]=new int [TAMX][TAMY];
  
    
    //---------------------------------
    
    //Matriz A
    
    public static void crear_matriz(){
        
        Scanner dato = new Scanner (System.in);
        System.out.println("Escribir datos de la matriz ");

        for (i=0;i<TAMX;i++){
            for (j=0;j<TAMY;j++){
                System.out.print("Escribir valor " + i + " , " + j + " : ");
                try{
                MATRIZ [i][j] = dato.nextInt();
                }catch(InputMismatchException ex){
                           System.out.print("  No es entero fin de programa");
                           System.exit(0);
                
                }
            }
        }

    }
    
    
    public static void imprimir_matriz(){
          for (i=0;i<TAMX;i++){
             for (j=0;j<TAMY;j++){
                System.out.print(MATRIZ[i][j]+" ");
                }
                System.out.println(" ");
           }
    
    
    }
    
    //-----------------------------------------------------------------
    //Columna b
    
    public static void crear_Columna(){
        
        Scanner dato = new Scanner (System.in);
        System.out.println("Escribir la columna ");

        for (i=0;i<COLUM;i++){
            for (j=0;j<COLUM-1;j++){
                System.out.print("Escribir valor " + i + " , " + j + " : ");
                 try{
                COLUMNA [j][i] = dato.nextInt();
                 }catch(InputMismatchException ex){
                           System.out.print("  No es entero fin de programa");
                           System.exit(0);
                
                }
            }
        }

    }
    
    
    public static void imprimir_columna(){
           for (i=0;i<COLUM;i++){
             for (j=0;j<COLUM-1;j++){
     
                System.out.print(COLUMNA[j][i]+" ");
                }
                System.out.println(" ");
           }
    
    
    }
    
    //---------------------------------------------------------------
    //Matriz A
    
    public static void li(){
        int suma=0;
        

         for (i=0;i<TAMX;i++){
             for (j=0;j<TAMY;j++){           
                 suma =suma+MATRIZ[j][i];
           }
            
            if(suma==0){
                System.out.println("false hay columna de 0's");
                System.out.println("Programa finalizado finaliza aca por haber columnas de cero's");
               bit=0;

                System.exit(0);
            }
            else{
                System.out.println("Verdadero: No Cero's");
                 bit=1;
            }
        
           System.out.println(suma);
           suma=0;
        
      }
   }
    
    
    //-------------------------------------------------
    
    //Rango A
    
    public static int rango(){
    
        int suma=0;
        int resultado=0;
         for (i=0;i<TAMX;i++){
             for (j=0;j<TAMY;j++){                  
                 if(MATRIZ[i][j]==0){
                    suma=suma+0;
                 } 
            else{
                suma=suma+1;
                
            }
           }    
                 if(suma!=0){
                        resultado=resultado+1;
                 }
    } 
        //System.out.println(resultado);
            suma=0;
        return resultado;
   }
    
   public static int rangoResultante(){
       int ini=0;
       int resultado=rango();
       if(MATRIZ[TAMX-1][ini]==0 && MATRIZ[TAMX-1][ini+1]==0){
                     resultado=resultado-1;
                 }else{
       resultado=resultado+0;
       }
       
       if(resultado<0){
           resultado=0;
       }
       
       System.out.println(resultado);
            return resultado;
   }
   
   
   
   // Matriz asociada
    public static void matrizAsociada(){
            
        for (i=0;i<TAMX;i++){
             for (j=0;j<TAMY;j++){
                 
     
                 
            
                 
                 MATRIZASOCIADA[i][j]=MATRIZ[i][j]+(MATRIZ[i][j]*COLUMNA[j][i]) ;
                 
                 
        
                 
         
             }
         
        }
        
        
    
    }
    
    public static void imprimir_matrizAsociada(){
          for (i=0;i<TAMX;i++){
             for (j=0;j<TAMY;j++){
                 
                   
                    System.out.print(MATRIZASOCIADA[i][j]+" ");
                }
                System.out.println(" ");
           }
          
    
    
    }
    
    
    //---------------------------------------------------------------
    
    public static void main(String[] args) {
        
        crear_matriz();
        imprimir_matriz();
        
        crear_Columna();
        imprimir_columna();
        
        matrizAsociada();
        imprimir_matrizAsociada();
        
       
       
    //   principal p= new principal();
      // p.show();
       
        
        
       //li();
       //System.out.print(bit);
       // rango();
        //rangoResultante();
 
        
    }
    
}
